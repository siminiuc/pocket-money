import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:intl/intl.dart';
import 'package:pocket_money/app/models/prize.dart';
import 'package:provider/provider.dart';

import '../services/db.dart';

class PrizeHistory extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    initializeDateFormatting();
    final _db = Provider.of<Database>(context, listen: false);
    final _dateFormat = new DateFormat('yyyy-MM-dd');
    return FutureBuilder<List<Prize>>(
      future: _db.prizes,
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          final List<Prize> _prizesList = snapshot.data;
          return MaterialApp(
            home: Scaffold(
              appBar: AppBar(
                title: Text('Prize History'),
              ),
              body: Container(
                child: Column(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Expanded(
                      child: ListView.builder(
                          itemCount: _prizesList.length,
                          itemBuilder: (context, index) {
                            var _item = _prizesList[index];
                            return Container(
                              padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
                              height: 150,
                              width: double.maxFinite,
                              child: Card(
                                elevation: 5,
                                child: Padding(
                                  padding: EdgeInsets.all(7),
                                  child: Stack(children: <Widget>[
                                    Align(
                                      alignment: Alignment.centerRight,
                                      child: Stack(
                                        children: <Widget>[
                                          Padding(
                                            padding: const EdgeInsets.only(
                                                left: 10, top: 5),
                                            child: Column(
                                              children: <Widget>[
                                                Row(
                                                  children: <Widget>[
                                                    _shopIcon(
                                                      _item.shopIconURL,
                                                    ),
                                                    SizedBox(
                                                      height: 10,
                                                    ),
                                                    _shopName(
                                                      _item.shop,
                                                    ),
                                                    Spacer(),
                                                    _prizeDate(
                                                      _dateFormat,
                                                      _item.wonDate,
                                                    ),
                                                    SizedBox(
                                                      width: 10,
                                                    ),
                                                    _calendarIcon(),
                                                    SizedBox(
                                                      width: 20,
                                                    ),
                                                  ],
                                                ),
                                                Row(
                                                  children: <Widget>[
                                                    _couponCode(
                                                      _item.couponCode,
                                                      _item.description,
                                                    )
                                                  ],
                                                ),
                                              ],
                                            ),
                                          )
                                        ],
                                      ),
                                    )
                                  ]),
                                ),
                              ),
                            );
                          }),
                    ),
                  ],
                ),
              ),
            ),
          );
        } else
          return MaterialApp(
            home: Scaffold(
              appBar: AppBar(
                title: Text('Prize History'),
              ),
              body: Container(
                child: Column(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Expanded(
                      child: ListView(),
                    ),
                  ],
                ),
              ),
            ),
          );
      },
    );
  }

  Widget _shopIcon(String iconURL) {
    return Padding(
      padding: const EdgeInsets.only(
        right: 10,
      ),
      child: Align(
        alignment: Alignment.centerLeft,
        child: CachedNetworkImage(
          imageUrl: iconURL,
          width: 30,
          height: 30,
        ),
      ),
    );
  }

  Widget _shopName(String shopName) {
    return Align(
      alignment: Alignment.centerLeft,
      child: RichText(
        text: TextSpan(
          text: shopName,
          style: TextStyle(
            fontWeight: FontWeight.bold,
            color: Colors.black,
            fontSize: 20,
          ),
        ),
      ),
    );
  }

  Widget _prizeDescription(String description) {
    return Align(
      alignment: Alignment.bottomRight,
      child: RichText(
        text: TextSpan(
          text: description,
          style: TextStyle(
            fontWeight: FontWeight.bold,
            color: Colors.grey,
            fontSize: 15,
          ),
        ),
      ),
    );
  }

  Widget _prizeDate(DateFormat format, Timestamp date) {
    return Align(
      alignment: Alignment.topRight,
      child: RichText(
        text: TextSpan(
          text: '${format.format(date.toDate())}',
          style: TextStyle(
            fontWeight: FontWeight.bold,
            color: Colors.green,
            fontSize: 15,
          ),
        ),
      ),
    );
  }

  Widget _calendarIcon() {
    return Align(
      alignment: Alignment.topRight,
      child: Icon(
        Icons.calendar_today,
        color: Colors.green,
        size: 30,
      ),
    );
  }

  Widget _couponCode(String couponCode, String prizeDescription) {
    return Align(
      alignment: Alignment.centerLeft,
      child: Padding(
        padding: const EdgeInsets.only(
          top: 20.0,
        ),
        child: Row(
          children: <Widget>[
            RichText(
              textAlign: TextAlign.left,
              text: TextSpan(
                text: couponCode,
                style: TextStyle(
                  color: Colors.black,
                  fontSize: 35,
                ),
              ),
            ),
            SizedBox(
              width: 55,
            ),
            _prizeDescription(prizeDescription),
          ],
        ),
      ),
    );
  }
}
