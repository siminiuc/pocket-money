import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:pocket_money/app/models/deal.dart';
import 'package:pocket_money/app/models/prize.dart';
import 'package:provider/provider.dart';
import 'package:pocket_money/app/services/db.dart';
import 'package:pocket_money/app/ui/common_widgets/platform_alert_dialog.dart';

class Offers extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
        fontFamily: "Montserrat",
      ),
      home: MyHomePage(title: 'Offers'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: _buildGridView(),
    );
  }

  FutureBuilder<List<Deal>> _buildGridView() {
    final _db = Provider.of<Database>(context, listen: false);
    return FutureBuilder<List<Deal>>(
      future: _db.deals,
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          final List<Deal> _dealsList = snapshot.data;
          return GridView.builder(
              padding: const EdgeInsets.all(4.0),
              gridDelegate:
                  SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2),
              itemCount: _dealsList.length,
              itemBuilder: (context, index) {
                var _item = _dealsList[index];
                return Card(
                  elevation: 4.0,
                  child: Stack(
                    fit: StackFit.loose,
                    alignment: Alignment.center,
                    children: <Widget>[
                      Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          GestureDetector(
                            child: AspectRatio(
                              aspectRatio: 4 / 2,
                              child: CachedNetworkImage(
                                imageUrl: _item.imageURL,
                              ),
                            ),
                            onTap: () => _purchaseConfirmation(
                              context,
                              new Prize(
                                description: _item.prize,
                                price: _item.price,
                                shop: _item.shop,
                                couponCode: 'ABC',
                                shopIconURL: _item.imageURL,
                              ),
                            ),
                          ),
                          Text(
                            '${_item.shop} ${_item.prize}',
                            textAlign: TextAlign.center,
                            style: Theme.of(context).textTheme.subtitle1,
                          )
                        ],
                      ),
                      Padding(
                        padding: const EdgeInsets.only(
                          right: 8.0,
                          bottom: 8.0,
                        ),
                        child: Align(
                          alignment: Alignment.bottomRight,
                          child: Text('${_item.price} Ștefănei'),
                        ),
                      ),
                    ],
                  ),
                );
              });
        } else
          return CircularProgressIndicator();
      },
    );
  }
}

Future<void> _purchaseConfirmation(BuildContext context, Prize prize) async {
  final _db = Provider.of<Database>(context, listen: false);
  int _currentCoins;
  await _db.userCoins.then((value) => _currentCoins = value);
  if (_currentCoins >= prize.price) {
    await _db.decrementCoins(prize);
    await PlatformAlertDialog(
      title: 'Congratulations!',
      content:
          'Your code is 81SBA19K. You will receive a confirmation email soon',
      defaultActionText: 'Great!',
    ).show(context);
  } else {
    int _difference = prize.price - _currentCoins;
    await PlatformAlertDialog(
      title: 'Sorry!',
      content: 'You need $_difference more Ștefănei',
      defaultActionText: 'Ok',
    ).show(context);
  }
}
