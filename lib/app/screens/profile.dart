import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:pocket_money/app/screens/prizes.dart';
import 'package:provider/provider.dart';
import 'package:pocket_money/app/services/auth.dart';
import 'package:pocket_money/app/services/db.dart';
import 'package:pocket_money/app/ui/common_widgets/platform_alert_dialog.dart';

import '../services/auth.dart';

class Profile extends StatelessWidget {
  Future<void> _signOut(BuildContext context) async {
    try {
      final _auth = Provider.of<Auth>(context, listen: false);
      await _auth.signOut();
    } catch (e) {
      print(e.toString());
    }
  }

  Future<void> _confirmSignOut(BuildContext context) async {
    final _didRequestSignOut = await PlatformAlertDialog(
      title: 'Logout',
      content: 'Are you sure that you want to log out?',
      cancelActionText: 'Cancel',
      defaultActionText: 'Logout',
    ).show(context);
    if (_didRequestSignOut == true) {
      _signOut(context);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Profile'),
        actions: <Widget>[
          FlatButton(
            child: Text(
              'Logout',
              style: TextStyle(fontSize: 18, color: Colors.white),
            ),
            onPressed: () => _confirmSignOut(context),
          ),
        ],
      ),
      body: _buildContents(context),
    );
  }

  Widget _buildContents(BuildContext context) {
    final _db = Provider.of<Database>(context, listen: false);
    return Scaffold(
      body: Column(
        children: <Widget>[
          Container(
            width: MediaQuery.of(context).size.width,
            height: 360,
            child: Center(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  FutureBuilder<String>(
                      future: _db.userAvatarURL,
                      builder: (context, snapshot) {
                        if (snapshot.hasData) {
                          final String _url = snapshot.data;
                          return CachedNetworkImage(
                            imageUrl: _url,
                            imageBuilder: (context, imageProvider) => Container(
                              width: 80.0,
                              height: 80.0,
                              decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                image: DecorationImage(
                                    image: imageProvider, fit: BoxFit.cover),
                              ),
                            ),
                            placeholder: (context, url) =>
                                CircularProgressIndicator(),
                            errorWidget: (context, url, error) =>
                                Icon(Icons.error),
                          );
                        } else
                          return CircularProgressIndicator();
                      }),
                  SizedBox(
                    height: 10.0,
                  ),
                  FutureBuilder<String>(
                      future: _db.userFullName,
                      builder: (context, snapshot) {
                        if (snapshot.hasData) {
                          final String _displayName = snapshot.data;
                          return Text(
                            _displayName,
                            textAlign: TextAlign.center,
                            style:
                                TextStyle(fontSize: 22.0, color: Colors.black),
                          );
                        } else
                          return CircularProgressIndicator();
                      }),
                  SizedBox(
                    height: 10.0,
                  ),
                  Card(
                    margin:
                        EdgeInsets.symmetric(horizontal: 20.0, vertical: 5.0),
                    clipBehavior: Clip.antiAlias,
                    color: Colors.white,
                    elevation: 5.0,
                    child: Padding(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 8.0, vertical: 22.0),
                      child: Row(
                        children: <Widget>[
                          Expanded(
                            child: Column(
                              children: <Widget>[
                                Text(
                                  "Ștefănei",
                                  style: TextStyle(
                                    color: Colors.redAccent,
                                    fontSize: 22.0,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                                SizedBox(
                                  height: 5.0,
                                ),
                                FutureBuilder<int>(
                                    future: _db.userCoins,
                                    builder: (context, snapshot) {
                                      if (snapshot.hasData) {
                                        final int _coinsValue = snapshot.data;
                                        return Text(
                                          '$_coinsValue',
                                          style: TextStyle(
                                            fontSize: 20.0,
                                            color: Colors.pinkAccent,
                                          ),
                                        );
                                      } else
                                        return CircularProgressIndicator();
                                    }),
                              ],
                            ),
                          ),
                          Expanded(
                            child: Column(
                              children: <Widget>[
                                Text(
                                  "Quizzes",
                                  style: TextStyle(
                                    color: Colors.redAccent,
                                    fontSize: 22.0,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                                SizedBox(
                                  height: 5.0,
                                ),
                                FutureBuilder<int>(
                                    future: _db.userQuizzesCompleted,
                                    builder: (context, snapshot) {
                                      if (snapshot.hasData) {
                                        int _quizesCompleted = snapshot.data;
                                        return Text(
                                          '$_quizesCompleted',
                                          style: TextStyle(
                                            fontSize: 20.0,
                                            color: Colors.pinkAccent,
                                          ),
                                        );
                                      } else
                                        return CircularProgressIndicator();
                                    }),
                              ],
                            ),
                          ),
                          Expanded(
                            child: Column(
                              children: <Widget>[
                                GestureDetector(
                                  child: Text(
                                    "Prizes",
                                    style: TextStyle(
                                      color: Colors.redAccent,
                                      fontSize: 22.0,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                  onTap: () => Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                      builder: (context) => PrizeHistory(),
                                    ),
                                  ),
                                ),
                                SizedBox(
                                  height: 5.0,
                                ),
                                FutureBuilder<int>(
                                    future: _db.userPrizesReedemed,
                                    builder: (context, snapshot) {
                                      if (snapshot.hasData) {
                                        int _prizesReedemed = snapshot.data;
                                        return Text(
                                          '$_prizesReedemed',
                                          style: TextStyle(
                                            fontSize: 20.0,
                                            color: Colors.pinkAccent,
                                          ),
                                        );
                                      } else
                                        return CircularProgressIndicator();
                                    }),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
