import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:pocket_money/app/models/deal.dart';
import 'package:pocket_money/app/models/prize.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:pocket_money/app/services/api.dart';

abstract class Database {
  Future<String> get userFullName;
  Future<String> get userEmail;
  Future<String> get userAvatarURL;
  Future<int> get userCoins;
  Future<int> get userQuizzesCompleted;
  Future<int> get userPrizesReedemed;
  Future<List<Deal>> get deals;
  Future<List<Prize>> get prizes;

  Future<void> incrementQuizzes();
  Future<void> incrementCoins(int value);
  Future<void> decrementCoins(Prize prize);
}

class FirestoreDatabase implements Database {
  FirestoreDatabase({@required this.uid}) : assert(uid != null);
  final String uid;
  final _db = FirebaseFirestore.instance;

  @override
  Future<int> get userQuizzesCompleted async {
    int _totalQuizzes;
    await _db
        .doc('users/$uid')
        .get()
        .then((value) => _totalQuizzes = value.data()['quizzesCompleted']);
    return _totalQuizzes;
  }

  @override
  Future<int> get userPrizesReedemed async {
    int _totalPrizesReedemed;
    await _db
        .doc('users/$uid')
        .collection('prizes')
        .get()
        .then((value) => _totalPrizesReedemed = value.docs.length);
    return _totalPrizesReedemed;
  }

  @override
  Future<String> get userEmail async {
    final _pref = await SharedPreferences.getInstance();
    return _pref.getString('email');
  }

  @override
  Future<String> get userFullName async {
    final _pref = await SharedPreferences.getInstance();
    return _pref.getString('displayName');
  }

  @override
  Future<int> get userCoins async {
    int _coinsValue;
    await _db
        .doc('users/$uid')
        .get()
        .then((value) => _coinsValue = value.data()['coins']);
    return _coinsValue;
  }

  @override
  Future<String> get userAvatarURL async {
    final _pref = await SharedPreferences.getInstance();
    return _pref.getString('avatarURL');
  }

  @override
  Future<void> incrementCoins(int value) async {
    await _db.doc('users/$uid').update({
      'coins': FieldValue.increment(value),
    });
  }

  @override
  Future<void> decrementCoins(Prize prize) async {
    await _db.doc('users/$uid').update({
      'coins': FieldValue.increment(-prize.price),
    });
    await _db.doc('users/$uid').collection('prizes').add(prize.toMap());
    final _pref = await SharedPreferences.getInstance();
    sendEmail(_pref.getString('email'), prize.shop,
        _pref.getString('displayName'), prize.description);
  }

  @override
  Future<List<Deal>> get deals async {
    List<Deal> _deals;
    await _db.collection('deals').get().then((value) => {
          _deals = value.docs
              .map(
                (deal) => Deal(
                  prize: deal.data()['prize'],
                  shop: deal.data()['shop'],
                  price: deal.data()['price'],
                  imageURL: deal.data()['image'],
                ),
              )
              .toList()
        });
    return _deals;
  }

  @override
  Future<void> incrementQuizzes() async {
    await _db.doc('users/$uid').update({
      'quizzesCompleted': FieldValue.increment(1),
    });
  }

  @override
  Future<List<Prize>> get prizes async {
    List<Prize> _prizes;
    await _db.doc('users/$uid').collection('prizes').get().then((value) => {
          _prizes = value.docs
              .map(
                (prize) => Prize(
                  description: prize.data()['description'],
                  price: prize.data()['price'],
                  shop: prize.data()['shop'],
                  wonDate: prize.data()['date'],
                  couponCode: prize.data()['couponCode'],
                  shopIconURL: prize.data()['shopIcon'],
                ),
              )
              .toList()
        });
    return _prizes.length > 0 ? _prizes : null;
  }
}
