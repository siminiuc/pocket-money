import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_facebook_login/flutter_facebook_login.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Auth {
  final GoogleSignIn _googleSignIn = GoogleSignIn();
  final FacebookLogin _facebookLogin = FacebookLogin();
  final _auth = FirebaseAuth.instance;
  final _db = FirebaseFirestore.instance;

  // Firebase user a realtime stream
  Stream<User> get user => _auth.authStateChanges();

  /// Sign in with Google
  Future<User> googleSignIn() async {
    try {
      final _googleSignInAccount = await _googleSignIn.signIn();
      final _googleAuth = await _googleSignInAccount.authentication;

      final _credential = GoogleAuthProvider.credential(
        accessToken: _googleAuth.accessToken,
        idToken: _googleAuth.idToken,
      );

      final _result = await _auth.signInWithCredential(_credential);
      User _user = _result.user;

      // Update user data
      updateUserData(_user);

      return _user;
    } catch (error) {
      print(error);
      return null;
    }
  }

  Future<User> signInWithFacebook() async {
    try {
      final _result = await _facebookLogin.logIn(['public_profile']);
      final _authResult = await _auth.signInWithCredential(
          FacebookAuthProvider.credential(_result.accessToken.token));
      User _user = _authResult.user;

      // Update user data
      await updateUserData(_user);

      return _user;
    } catch (error) {
      print(error);
      return null;
    }
  }

  /// Updates the User's data in Firestore on each new login
  Future<void> updateUserData(User user) async {
    final String _uid = user.uid;
    final _snapshot = await _db.doc('users/$_uid').get();
    final _prefs = await SharedPreferences.getInstance();

    _prefs.setString('avatarURL', user.photoURL);
    _prefs.setString('displayName', user.displayName);
    _prefs.setString('email', user.email);

    if (!_snapshot.exists) {
      await _db.doc('users/$_uid').set({
        'creationTime': user.metadata.creationTime,
        'phoneNumber': user.phoneNumber,
        'avatarURL': user.photoURL,
        'emailVerified': user.emailVerified,
        'email': user.email,
        'lastSignInTime': user.metadata.lastSignInTime,
        'displayName': user.displayName,
        'coins': 0,
        'quizzesCompleted': 0,
        'prizesReedemed': 0,
      });
    } else
      await _db.doc('users/$_uid').update({
        'lastSignInTime': user.metadata.lastSignInTime,
      });
  }

  // Sign out
  Future<void> signOut() {
    return _auth.signOut();
  }
}
