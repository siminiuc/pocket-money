import 'dart:convert';
import 'package:flutter/services.dart' show rootBundle;
import 'package:http/http.dart' as _http;
import 'package:pocket_money/app/models/category.dart';
import 'package:pocket_money/app/models/question.dart';

const String _baseURL = "https://opentdb.com/api.php";
const String _sendMailFunctionURL =
    "https://us-central1-smart-booking-282b7.cloudfunctions.net/sendMail";

Future<void> sendEmail(
    String dest, String shop, String destName, String prize) async {
  final String _url =
      "$_sendMailFunctionURL?dest=$dest&shop=$shop&destName=$destName&prize=$prize";
  _http.get(_url);
}

Future<List<Question>> getQuestions(
    Category category, int total, String difficulty) async {
  if (category.id == 1) {
    String _source;
    await rootBundle
        .loadString('assets/local_quizes/entrepreneurship.json')
        .then((_value) => _source = _value);
    final List<Map<String, dynamic>> questions =
        List<Map<String, dynamic>>.from(json.decode(_source)["results"]);
    return Question.fromData(questions);
  } else {
    String _url = "$_baseURL?amount=$total&category=${category.id}";
    if (difficulty != null) {
      _url = "$_url&difficulty=$difficulty";
    }
    final _http.Response _res = await _http.get(_url);
    final List<Map<String, dynamic>> _questions =
        List<Map<String, dynamic>>.from(json.decode(_res.body)["results"]);
    return Question.fromData(_questions);
  }
}
