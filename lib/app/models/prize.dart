import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';

class Prize {
  Prize({
    @required this.description,
    @required this.price,
    @required this.shop,
    @required this.couponCode,
    @required this.shopIconURL,
    this.wonDate,
  });
  final String description;
  final int price;
  final String shop;
  final String couponCode;
  final String shopIconURL;
  final Timestamp wonDate;

  Map<String, dynamic> toMap() {
    return {
      'description': description,
      'price': price,
      'shop': shop,
      'couponCode': couponCode,
      'shopIcon': shopIconURL,
      'date': Timestamp.now(),
    };
  }
}
