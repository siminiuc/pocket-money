import 'package:flutter/cupertino.dart';

class Deal {
  Deal({
    @required this.prize,
    @required this.shop,
    @required this.price,
    @required this.imageURL,
  });
  final String prize;
  final String shop;
  final int price;
  final String imageURL;

  Map<String, dynamic> toMap() {
    return {
      'prize': prize,
      'shop': shop,
      'price': price,
    };
  }
}
