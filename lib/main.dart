import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'app/screens/landing_page.dart';
import 'app/services/auth.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  runApp(PocketMoney());
}

class PocketMoney extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: Firebase.initializeApp(),
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.done) {
          return Provider<Auth>(
            create: (context) => Auth(),
            child: MaterialApp(
                title: 'Pocket Money',
                theme: ThemeData(primarySwatch: Colors.blue),
                home: LandingPage()),
          );
        }
        return CircularProgressIndicator();
      },
    );
  }
}
